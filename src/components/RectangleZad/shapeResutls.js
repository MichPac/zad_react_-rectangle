import React,{ useState,useEffect }  from 'react'


const ShapeResult = ({ count }, ref) => {
  let innerRef = React.useRef(ref);
  const [countState, setCountState] = useState(0);
  const [width, setWidth] = useState(100);
  const [height, setHeight] = useState(100);
  const obj = width*height
  
  useEffect(() => {
    innerRef = ref;
    console.log(innerRef.current);
    setWidth(innerRef.current.getAttribute("width"));
    setHeight(innerRef.current.getAttribute("height"));
    setCountState(count);
  }, [count, ref]);

  return (
    <div className="shape-result">
      <p>volume equals: {obj}</p>
    </div>
  );
};
const ForwardedResult = React.forwardRef(ShapeResult);
export default ForwardedResult;