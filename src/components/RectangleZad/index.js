import React,{ useState,useRef } from 'react'
import styled from 'styled-components'
import ShapeResult from './shapeResutls'


const Shape = styled.div`
width:${size => size.width+'px'};
height:${size => size.height+'px'};
background-color:lightcoral;
display:flex;
text-align:center;
align-items: center;
justify-content: center;
transition: all 0.5s;
`;

const StyledCompW = () => {
    const [size, setSize] = useState({ width: 300, height: 300 });
    const [count, setCount] = useState(0);
  
    let ref = useRef(<Shape/>);
    
  
    const handleResize = () => {
      const max = 500;
      const min = 100;
      let width = Math.floor(Math.random() * (max - min)) + min;
      let height = Math.floor(Math.random() * (max - min)) + min;
      setSize({ width, height });
      setCount(count+1);
    };
    return (
      <div className="styled-component">
        <Shape height={size.height} width={size.width} ref={ref}>
        <ShapeResult count={count} {...{ ref }}/>
        </Shape>
        <p>Width: {size.width} px</p>
        <p>Height: {size.height} px</p>
        <button onClick={handleResize}>Generate</button>
      </div>
    );
  };

  export default StyledCompW